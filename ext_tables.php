<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_taxonomy', 'Configuration/TypoScript', 'hive_ext_taxonomy');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveexttaxonomy_domain_model_tag', 'EXT:hive_ext_taxonomy/Resources/Private/Language/locallang_csh_tx_hiveexttaxonomy_domain_model_tag.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveexttaxonomy_domain_model_tag');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            hive_ext_taxonomy,
            'tx_hiveexttaxonomy_domain_model_tag'
        );

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder